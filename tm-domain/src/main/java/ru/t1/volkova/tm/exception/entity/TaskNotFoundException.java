package ru.t1.volkova.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("Error! Task Not found...");
    }

}
