package ru.t1.volkova.tm.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.volkova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.dto.IProjectDTOService;
import ru.t1.volkova.tm.api.service.dto.ITaskDTOService;
import ru.t1.volkova.tm.api.service.dto.IUserDTOService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.*;
import ru.t1.volkova.tm.dto.model.TaskDTO;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.repository.dto.ProjectDTORepository;
import ru.t1.volkova.tm.repository.dto.TaskDTORepository;
import ru.t1.volkova.tm.repository.dto.UserDTORepository;
import ru.t1.volkova.tm.service.*;
import ru.t1.volkova.tm.service.dto.ProjectDTOService;
import ru.t1.volkova.tm.service.dto.TaskDTOService;
import ru.t1.volkova.tm.service.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;

public class TaskServiceTest {

    private static String USER_ID;

    private static String USER_TEST_ID;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final EntityManager entityManager = connectionService.getEntityManager();

    @NotNull
    final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);

    @NotNull
    final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);

    @NotNull
    final IUserDTORepository userRepository = new UserDTORepository(entityManager);

    @Nullable
    private List<TaskDTO> taskList;

    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(projectRepository, connectionService);

    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(taskRepository, connectionService);

    @NotNull
    private final IUserDTOService userService = new UserDTOService(userRepository, connectionService, propertyService, projectService, taskService);

    @Before
    public void initRepository() {
        @Nullable final UserDTO user = userService.findByLogin("user");
        @Nullable final UserDTO userTest = userService.findByLogin("test");
        if (user == null || userTest == null || taskList == null) return;
        USER_ID = user.getId();
        USER_TEST_ID = userTest.getId();
        taskList = taskService.findAll(USER_ID);
    }

    @Test
    public void testCreate(
    ) {
        int size = taskService.getSize(USER_ID);
        @NotNull final TaskDTO task = taskService.create(USER_ID, "new_task", "new description");
        Assert.assertEquals(task, taskService.findOneById(USER_ID, task.getId()));
        Assert.assertEquals(size + 1, taskService.getSize(USER_ID));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateUserId(
    ) {
        taskService.create(null, "new_task", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateNameEmpty(
    ) {
        taskService.create(USER_ID, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateDescriptionEmpty(
    ) {
        taskService.create(USER_TEST_ID, "new", null);
    }

    @Test
    public void testUpdateById() {
        @NotNull final String newName = "new taskUpd";
        @NotNull final String newDescription = "new taskUpd";
        if (taskList == null) return;
        @Nullable final TaskDTO task = taskList.get(0);
        if (task == null) return;
        taskService.updateById(task.getUserId(), task.getId(), newName, newDescription);
        @Nullable final TaskDTO taskUpd = taskService.findOneById(task.getUserId(), task.getId());
        if (taskUpd == null) return;
        Assert.assertEquals(newName, taskUpd.getName());
        Assert.assertEquals(newDescription, taskUpd.getDescription());
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateNotFoundTask(
    ) {
        @NotNull final String newName = "new task";
        @NotNull final String newDescription = "new task";
        @NotNull final String id = "non-existent-id";
        taskService.updateById(USER_ID, id, newName, newDescription);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) {
        taskService.updateById(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmpty(
    ) {
        if (taskList == null) return;
        taskService.updateById(taskList.get(0).getUserId(), taskList.get(0).getId(), null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmpty(
    ) {
        if (taskList == null) return;
        taskService.updateById(taskList.get(0).getUserId(), taskList.get(0).getId(), "name", null);
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String newName = "new nameUpd";
        @NotNull final String newDescription = "new descUpd";
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        taskService.updateByIndex(USER_ID, index, newName, newDescription);
        @Nullable final TaskDTO task = taskService.findOneByIndex(USER_ID, index);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateIdEmptyByIndex(
    ) {
        taskService.updateByIndex(USER_ID, 10, "name", "new description");
        taskService.updateByIndex(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmptyByIndex(
    ) {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        taskService.updateByIndex(USER_ID, index, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmptyByIndex(
    ) {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        taskService.updateByIndex(USER_TEST_ID, index, "name", null);
    }

    @Test
    public void testChangeStatusById() {
        if (taskList == null) return;
        @NotNull final String id = taskList.get(0).getId();
        @NotNull final String userId = taskList.get(0).getUserId();
        @Nullable final TaskDTO task = taskService.changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeStatusIdEmptyById() {
        taskService.changeTaskStatusById(USER_ID, null, Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID, "", Status.IN_PROGRESS);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusTaskNotFoundById() {
        taskService.changeTaskStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testChangeStatusByIndex() {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        @Nullable final TaskDTO task = taskService.changeTaskStatusByIndex(USER_ID, index, Status.IN_PROGRESS);
        if (task == null) return;
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusIndexExceptionByIndex() {
        taskService.changeTaskStatusByIndex(USER_ID, 22, Status.IN_PROGRESS);
        taskService.changeTaskStatusByIndex(USER_ID, null, Status.IN_PROGRESS);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusTaskNotFoundByIndex() {
        taskService.changeTaskStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testFindOneById() {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        if (taskList == null) return;
        Assert.assertNotNull(taskService.findOneById(USER_ID, taskList.get(index).getId()));
    }

    @Test(expected = TaskNotFoundException.class)
    public void testFindOneByIdNegative() {
        Assert.assertNotNull(taskService.findOneById(USER_ID, "non-existent"));
    }

    @Test
    public void testFindAllByProjectId() {
        if (taskList == null) return;
        @Nullable String projectId = null;
        @Nullable String userId = null;
        for (@NotNull final TaskDTO task : taskList) {
            if (task.getProjectId() == null) continue;
            projectId = task.getProjectId();
            userId = task.getUserId();
            break;
        }
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        Assert.assertNotNull(tasks);
    }

    @Test
    public void findAllByProjectIdNegative() {
        if (taskList == null) return;
        @Nullable String userId = null;
        for (@NotNull final TaskDTO task : taskList) {
            userId = task.getUserId();
            break;
        }
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, "non-existent");
        if (tasks == null) return;
        Assert.assertEquals(0, tasks.size());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void findAllByProjectIdProjectNull() {
        taskService.findAllByProjectId(USER_ID, null);
        taskService.findAllByProjectId(USER_ID, "");
    }

}
