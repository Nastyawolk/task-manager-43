package ru.t1.volkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.repository.model.IAbstractRepository;
import ru.t1.volkova.tm.model.AbstractModel;

import javax.persistence.EntityManager;

public abstract class AbstractRepository<E extends AbstractModel> implements IAbstractRepository<E> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final E entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(@NotNull final E entity) {
        entityManager.merge(entity);
    }

}
