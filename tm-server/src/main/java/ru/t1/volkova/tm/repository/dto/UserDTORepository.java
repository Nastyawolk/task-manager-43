package ru.t1.volkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;
import ru.t1.volkova.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public final class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    public UserDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public UserDTO findOneByLogin(@NotNull final String login) {
        return entityManager
                .createQuery("SELECT e FROM UserDTO e WHERE e.login = :login", UserDTO.class)
                .setParameter("login", login)
                .getResultStream()
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findOneByEmail(@NotNull final String email) {
        return entityManager
                .createQuery("SELECT e FROM UserDTO e WHERE e.email = :email", UserDTO.class)
                .setParameter("email", email)
                .getResultStream()
                .findFirst().orElse(null);
    }


    @Override
    public @NotNull List<UserDTO> findAll() {
        return entityManager.createQuery("FROM UserDTO", UserDTO.class).getResultList();
    }

    @Override
    public @Nullable List<UserDTO> findAll(@NotNull final Comparator comparator) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM UserDTO ORDER BY e." + param, UserDTO.class)
                .getResultList();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM UserDTO").executeUpdate();
    }

    @Override
    @NotNull
    public UserDTO findOneById(@NotNull final String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Override
    public @Nullable UserDTO findOneByIndex(
            @NotNull final Integer index
    ) {
        return entityManager
                .createQuery("FROM UserDTO", UserDTO.class)
                .getResultStream()
                .skip(index).findFirst()
                .orElse(null);
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM UserDTO e WHERE e.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        @Nullable final UserDTO user = findOneByIndex(index);
        if (user == null) {
            return;
        }
        removeOneById(user.getId());
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM UserDTO e", Long.class)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

}
