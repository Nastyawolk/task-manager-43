package ru.t1.volkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.repository.model.IAbstractUserOwnedRepository;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IAbstractUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final M entity) {
        super.add(entity);
    }

    @Override
    public void update(@NotNull final M entity) {
        super.update(entity);
    }

}
