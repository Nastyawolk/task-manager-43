package ru.t1.volkova.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.volkova.tm.api.service.dto.IAbstractDTOService;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Getter
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IAbstractDTORepository<M>> implements IAbstractDTOService<M> {

    @NotNull
    protected final R repository;

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractDTOService(
            @NotNull final R repository,
            @NotNull final IConnectionService connectionService
    ) {
        this.repository = repository;
        this.connectionService = connectionService;
    }

    @Override
    public void add(@NotNull final M entity) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@NotNull final M entity) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull List<M> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable List<M> findAll(@NotNull final Comparator comparator) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull M findOneById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable M findOneByIndex(@NotNull final Integer index) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return repository.findOneByIndex(index);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneByIndex(index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

}
