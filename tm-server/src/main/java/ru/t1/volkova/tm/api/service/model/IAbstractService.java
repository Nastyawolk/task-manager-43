package ru.t1.volkova.tm.api.service.model;

import ru.t1.volkova.tm.api.repository.model.IAbstractRepository;
import ru.t1.volkova.tm.model.AbstractModel;

public interface IAbstractService<M extends AbstractModel> extends IAbstractRepository<M> {

}
